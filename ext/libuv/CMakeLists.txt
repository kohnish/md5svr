if(${USE_SYSTEM_LIBUV})
    pkg_check_modules(UV libuv REQUIRED)
    add_library(uv SHARED IMPORTED GLOBAL)
    set_target_properties(uv PROPERTIES IMPORTED_LOCATION ${pkgcfg_lib_UV_uv})
else()
    ExternalProject_Add(libuv
        GIT_REPOSITORY https://github.com/libuv/libuv
        GIT_TAG v1.40.0
        GIT_SHALLOW TRUE
        GIT_PROGRESS TRUE
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR} -DCMAKE_INSTALL_LIBDIR=${PROJECT_BINARY_DIR}/lib
    )
    add_library(uv STATIC IMPORTED GLOBAL)
    set_target_properties(uv PROPERTIES IMPORTED_LOCATION ${PROJECT_BINARY_DIR}/lib/libuv_a.a)
endif()

